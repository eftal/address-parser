# Federal State Finder
## Description
This tool can filtered only address data from `data/testdaten.txt` and make an api request to Google Maps Geocoding API than it returns federalState values into index.html

## Installation

1. `yarn install`

## Run the tool

1. `yarn webpack` (watcher)
