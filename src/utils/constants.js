export const ErrorMessages = {
  UNDEFINED_ENV_VAR: 'You have to set an API Key for google maps. If you want to more info please check readme file or run `yarn run help`'
}

export const KEYS = {
  GOOGLE_MAPS_API_KEY: 'GOOGLE_MAPS_API_KEY'
}