const readFile = (fileName) => {
  return fetch(fileName).then((response) => response.text());
};

const apiKey = '';

const getFederalState = async (address) => {
  if (!address || address.length == 0) return;
  let tempNameArr = [];
  await fetch(
    `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${apiKey}`
  )
    .then((response) => response.json())
    .then((data) => {
      if (data.results.length == 0) {
        console.warn(
          '[EFTAL] - You have an error from google maps API. Please check your message: ',
          data.error_message
        );
        return;
      }
      const result = data.results[0];
      const addressDetails = result.address_components;
      tempNameArr = addressDetails.filter((address) =>
        address.types.includes('locality')
      );
    });
  if (tempNameArr.length == 0) {
    return '[Please check developer tools console]';
  }
  return tempNameArr[0]['long_name'];
};

export { readFile, getFederalState };
