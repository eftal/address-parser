import { Regulator } from './classes/Regulator';
import { ErrorMessages, KEYS } from './utils/constants';
import { readFile, getFederalState } from './utils/helpers';

const app = document.getElementById('root');

// check credentials 
console.log(process);

readFile('../data/testdaten.txt').then(async(content) => {
  
  const regulator = new Regulator(content);
  const addressRecords = regulator.getAddressRecords();
  addressRecords.map(async(address) => {
    const element = document.createElement('pre');
    const federalState = await getFederalState(address)
    element.appendChild(document.createTextNode(`Address Data: ${address} - Federal State: ${federalState}`));
    app.appendChild(element);
  })  
});
