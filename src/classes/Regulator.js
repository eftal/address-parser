export class Regulator {

  constructor(listData) {
    this.listData = listData;
    this.addressLines = [];
    this.addressRegexForGermany = [
      /^\d{5}\s+[a-zA-ZÄÖÜäöüß -]+$/g,
      /^[a-zA-ZÄÖÜäöüß -]+\s[0-9]{0,2}$/g,
    ];
    this.cleaner();
  }

  cleaner() {
    const tempLines = this.listData.split('\n').filter((c) => c.length > 0);
    let tempDatas = [];
    tempLines.map((_, i) => {
      if (i % 3 == 0) {
        tempDatas.push(tempLines.slice(i - 3, i));
      }
    });
    this.listData = tempLines.filter((data) => data.length > 0);
  }

  getAddressRecords() {
    this.listData.map((data) => {
      if (this.addressRegexForGermany[0].exec(data) || this.addressRegexForGermany[1].exec(data)) {
        this.addressLines.push(data);
      }
    });
    return this.addressLines;
  }

}
